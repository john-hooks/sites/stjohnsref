---
title: "Mighty to Save"
date: 2018-05-13
author: Rev. Crawford
description : "Daniel 6"
---

{{< sermon Daniel6_201805 >}}

This sermon is based upon Daniel 6 and was preached by Rev. Crawford on May 13, 2018.

You can also download the audio file [here](https://archive.org/download/Daniel6_201805/Daniel_6.mp3).


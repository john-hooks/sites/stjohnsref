---
title: "The Authority of Christ"
date: 2024-04-07
author: Rev. Crawford
description : "Galatians 1:1-2"
---

{{< youtube BABGCex51YM >}}

We begin our series in Galatians with this sermon on 1:1-2.
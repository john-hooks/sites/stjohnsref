---
title: "Practice Peaceful Living"
date: 2020-09-13
author: Rev. Crawford
description : "1 Thessalonians 5:12-18"
---

{{< sermon practice-peaceful-living >}}

This sermon is based upon 1 Thessalonians 5:12ff. and was preached by Rev. Crawford on September 13, 2020.


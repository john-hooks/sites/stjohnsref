---
title: "Obedient Children"
date: 2018-02-04
author: Rev. Crawford
description : "1 Peter 1:13-16"
---

{{< sermon ObedientChildren >}}

This sermon is based upon 1 Peter 1:13-16 and was preached by Rev. Crawford on February 04, 2018.

You can also download the audio file [here](https://archive.org/download/ObedientChildren/Obedient_Children.mp3).


---
title: "Anger and Compassion"
date: 2018-09-23
author: Rev. Crawford
description : "Jonah 4"
---

{{< sermon MercyAndCompassionJonah4 >}}

This sermon is based upon Jonah 4 and was preached by Rev. Crawford on September 23, 2018.

You can also download the audio file [here](https://archive.org/download/MercyAndCompassionJonah4/Mercy_and_Compassion_Jonah_4.mp3).


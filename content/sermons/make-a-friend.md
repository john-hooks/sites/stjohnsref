---
title: "Make A Friend"
date: 2020-08-16
author: Rev. Crawford
description : "1 Thessalonians 2:17-3:13"
---

{{< sermon makea-friend >}}

This sermon is based upon 1 Thessalonians 2:17-3:13 and was preached by Rev. Crawford on August 16, 2020.

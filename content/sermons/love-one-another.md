---
title: "Love One Another"
date: 2018-02-18
author: Rev. Crawford
description : "1 Peter 1:22-23"
---

{{< sermon Perservere_201802 >}}

This sermon is based upon 1 Peter 1:22-23 and was preached by Rev. Crawford on February 18, 2018.

You can also download the audio file [here](https://archive.org/download/Persevere_201802/Persevere.mp3).


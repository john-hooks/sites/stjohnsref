---
title: "Reformed Theology: The Centrality of the World"
date: 2015-09-24
author: Rev. Crawford
description : "Reformed Theology"
---

When you ask someone what "Reformed" means, you could get a whole host of responses.  Some think that it is a defined liturgy (prayers, readings, saying, etc.), others think it's simply a name on the church, still others might simply tell you they have no idea.  While there are several distinctive themes of Reformed theology, the first one which stands out is the centrality of the Word of God.  In all matters of faith and life, we are to subject our thoughts and beliefs to Scripture.  Yes, our experiences, traditions, and reason all play a part in interpretation.  The distinctive of Reformed theology is that all of these are subjected to Scripture as the final authority.  As the Westminster Standards put it, "Under the name of Holy Scripture...are now contained the books of the Old and New Testament...all which are given by inspiration of God to be the rule of faith and life" (WCF I.1.2).  This is based upon the belief that all of Scripture is "inspired by God" (2 Tim. 3:16).

To have the Bible be the rule of faith means that what we believe concerning God grows out of the Bible and not our own thoughts.  Look at Greek/Roman mythology.  The gods on Olympus have animosity for one another, they attack each other, undermine one another, and plot against each other, in a chaotic pantheon.  They have to be pleased in order to reward the people.  This entire model is built upon the premise of creating gods in our image; it uses our experiences to form the idea of "gods."  Instead, when we read the Bible, we are given a different picture of God, his self-revelation.  We are then to conform our minds to his revelation not re-interpret passages so that they will fit what we 'expect' or 'want' him to be.

We also seek to conform our beliefs to the Bible when it comes to our actions.  If we were to use our reason as the basis for our lives, we may determine (combining our reason and experience) that 'it's a dog-eat-dog world,' and so we adopt the pirate mentality, "take what you can get and give nothing back."  Yet if we examine the Scriptures, we see that God's desire is for us to love him, first and fore-most, and then to love our neighbors as ourselves - hardly a 'dog-eat-dog' mentality.  We must, once again, conform our minds to the Word of God.

The Reformers of old expressed these ideas in the Latin phrase, sola Scripture, i.e., Scripture alone.  It is the Bible, not a man's opinion, not a council, not the church, which determines our beliefs, but the Word of God.  This centrality of the Word is one of the main distinctives of Reformed theology.

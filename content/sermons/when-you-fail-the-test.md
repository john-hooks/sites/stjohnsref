---
title: "When You Fail the Test"
date: 2020-06-21
author: Rev. Crawford
description : "Exodus 15-17"
---

{{< sermon when-we-fail >}}

This sermon is based upon Exodus 15-17 and was preached by Rev. Crawford on June 21, 2020.

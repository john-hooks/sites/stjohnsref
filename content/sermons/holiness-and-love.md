---
title: "Holiness and Love"
date: 2020-08-23
author: Rev. Crawford
description : "1 Thessalonians 4:1-12"
---

{{< sermon holinessand-love >}}

This sermon is based upon 1 Thessalonians 4:1-12 and was preached by Rev. Crawford on August 23, 2020.

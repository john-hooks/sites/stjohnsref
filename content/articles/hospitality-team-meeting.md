---
title: "Hospitality Team Meeting"
date: 2024-06-27
description : "Hospitality Team Meeting"
---

The hospitality team focuses on the welcoming atmosphere and life of the church. Our next hospitality team meeting will focus on personal responsibility when it comes to hospitality.

# When
July 7, 2024  
Following Worship
At the Church
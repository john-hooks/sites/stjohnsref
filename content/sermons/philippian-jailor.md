---
title: "Narrative: Philippian Jailor"
date: 2017-07-16
author: Rev. Crawford
description : "Acts 16:20-40"
---

{{< sermon PersonalEvangelism >}}

This sermon is based upon Acts 16:20-40 and was preached by Rev. Crawford on July 16, 2017.

You can also download the audio file [here](https://archive.org/download/PersonalEvangelism/Personal%20Evangelism.mp3).


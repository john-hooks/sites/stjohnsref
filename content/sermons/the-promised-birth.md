---
title: "The Promised Birth"
date: 2019-01-13
author: Rev. Crawford
description : "Genesis 21:1-7"
---

{{< sermon jan13birthisaacgen2117 >}}

This sermon is based upon Genesis 21:1-7 and was preached by Rev. Crawford on January 13, 2019.

You can also download the audio file [here](https://archive.org/download/jan13birthisaacgen2117/Jan%2013%20Birth%20Isaac%20Gen%2021%201-7.mp3).


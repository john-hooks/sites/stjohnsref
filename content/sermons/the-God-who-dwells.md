---
title: "The God Who Dwells"
date: 2020-07-26
author: Rev. Crawford
description : "Exodus 35-40"
---

{{< sermon the-god-who-dwells >}}

This sermon is based upon Exodus 35-40 and was preached by Rev. Crawford on July 26, 2020.

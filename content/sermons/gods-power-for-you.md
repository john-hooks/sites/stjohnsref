---
title: "God's Power For You"
date: 2019-02-17
author: Rev. Crawford
description : "Ephesians 1:15-23"
---

{{< sermon feb17ephesians1.1523 >}}

This sermon is based upon Ephesians 1:15-23 and was preached by Rev. Crawford on February 17, 2019.

You can also download the audio file [here](https://archive.org/download/feb17ephesians1.1523/Feb%2017%20Ephesians%201.15-23.mp3).

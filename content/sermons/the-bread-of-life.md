---
title: "The Bread of Life"
date: 2017-12-17
author: Rev. Crawford
description : "John 6"
---

{{< sermon TheBreadofLife_201712 >}}

This sermon is based upon John 6 and was preached by Rev. Crawford on December 17, 2017.

You can also download the audio file [here](https://archive.org/download/TheBreadofLife_201712/TheBreadofLife.mp3).


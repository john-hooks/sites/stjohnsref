---
title: "The God Who Forgives"
date: 2020-07-12
author: Rev. Crawford
description : "Exodus 32-34"
---

{{< sermon the-god-who-forgives >}}

This sermon is based upon Exodus 32-34 and was preached by Rev. Crawford on July 12, 2020.
